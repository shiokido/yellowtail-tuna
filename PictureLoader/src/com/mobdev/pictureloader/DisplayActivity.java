package com.mobdev.pictureloader;


import android.os.Bundle;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.view.Menu;
import android.widget.ImageView;

public class DisplayActivity extends Activity {
	
	private ImageView iView;
	private ProgressDialog dialog;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_display);
		
		iView = (ImageView)findViewById(R.id.imageView);
	    
		
		dialog = new ProgressDialog(this);
	    dialog.setTitle("Please Wait");
	    dialog.setMessage("The Bitmap is loading.");
	    
	    byte[] buffer = getIntent().getExtras().getByteArray("BITMAP");
		
	    Bitmap result  = BitmapFactory.decodeByteArray(buffer, 0, buffer.length);
	    
		/** Displaying the downloaded image */
		iView.setImageBitmap(result);		
		
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.display, menu);
		return true;
	}

}
