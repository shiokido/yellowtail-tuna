package com.mobdev.pictureloader;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.webkit.URLUtil;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	// DAO
	private UrlDAO dao;
	private ListView urlList;
	private Button startButton;
	private EditText editUrl;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		urlList = (ListView) findViewById(R.id.listUrlsMain);
		startButton = (Button) findViewById(R.id.bStart);
		editUrl = (EditText) findViewById(R.id.editUrl);

		// Create DAO object
		dao = new UrlDAO(this);

		// Set the list adapter and get TODOs list via DAO

		urlList.setAdapter(new ArrayAdapter<Url>(this,
				android.R.layout.simple_list_item_1, dao.getTodos()));

		urlList.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				// TODO Auto-generated method stub
				Url todo = (Url) parent.getAdapter().getItem(position);

				Toast.makeText(getApplicationContext(),
						"Selected " + todo.getText(), Toast.LENGTH_LONG).show();

				if (isNetworkAvailable()) {

					/** Creating a new non-ui thread task */
					DownloadTask downloadTask = new DownloadTask();
					
					ProgressDialog dialog = new ProgressDialog(MainActivity.this);
				    dialog.setTitle("Please Wait");
				    dialog.setMessage("The Bitmap is loading.");
					
					/** Starting the task created above */
					downloadTask.execute(todo.getText());

				} else {
					Toast.makeText(getBaseContext(),
							"Network is not Available", Toast.LENGTH_SHORT)
							.show();
				}

			}
		});

		startButton.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String url = new String(editUrl.getText().toString());

				editUrl.setText("");

				if (URLUtil.isHttpUrl(url) ) {

					// Add text to the database
					dao.createTodo(url);
					// Display success information
					Toast.makeText(getApplicationContext(), "New URL added!",
							Toast.LENGTH_LONG).show();
					
					urlList.setAdapter(new ArrayAdapter<Url>(MainActivity.this,
							android.R.layout.simple_list_item_1, dao.getTodos()));
					
					dao.close();

				} else {

					// Display fail information
					Toast.makeText(getApplicationContext(),
							"URL is not valid.", Toast.LENGTH_LONG).show();

				}

				

				

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;

	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

		switch (item.getItemId()) {
		case R.id.menu_map:

			// Create an intent
			Intent intent = new Intent(MainActivity.this, MapActivity.class);
			intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			startActivity(intent);

			return true;
		case R.id.menu_delete:
			//
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	private boolean isNetworkAvailable() {
		boolean available = false;
		/** Getting the system's connectivity service */
		ConnectivityManager connMgr = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

		/** Getting active network interface to get the network's status */
		NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();

		if (networkInfo != null && networkInfo.isAvailable())
			available = true;

		/** Returning the status of the network */
		return available;
	}

	private Bitmap downloadUrl(String strUrl) throws IOException {
		Bitmap bitmap = null;
		InputStream iStream = null;
		try {
			URL url = new URL(strUrl);
			
			
			/** Creating an http connection to communcate with url */
			HttpURLConnection urlConnection = (HttpURLConnection) url
					.openConnection();

			/** Connecting to url */
			urlConnection.connect();

			/** Reading data from url */
			iStream = urlConnection.getInputStream();

			/** Creating a bitmap from the stream returned from the url */
			bitmap = BitmapFactory.decodeStream(iStream);

		} catch (Exception e) {
			Log.d("Exception while downloading url", e.toString());
		} finally {
			iStream.close();
		}

		return bitmap;
	}

	private class DownloadTask extends AsyncTask<String, Integer, Bitmap> {
		Bitmap bitmap = null;

		@Override
		protected Bitmap doInBackground(String... url) {
			try {
				URLConnection connection = new URL(url[0]).openConnection();
				String contentType = connection.getHeaderField("Content-Type");
				boolean image = contentType.startsWith("image/");
				 if(image)
 {
					bitmap = downloadUrl(url[0]);
				 }
				 else {
					bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.noimage);
				}
			} catch (Exception e) {
				Log.d("Background Task", e.toString());
			}
			return bitmap;
		}

		@Override
		protected void onPostExecute(Bitmap result) {
			/**
			 * Getting a reference to ImageView to display the downloaded image
			 */

			ByteArrayOutputStream stream = new ByteArrayOutputStream();
			result.compress(Bitmap.CompressFormat.PNG, 100, stream);

			byte[] byteArray = stream.toByteArray();

			Intent intent = new Intent(MainActivity.this, DisplayActivity.class);

			intent.putExtra("BITMAP", byteArray);
			startActivity(intent);

			/** Showing a message, on completion of download process */
			Toast.makeText(getBaseContext(), "Image downloaded successfully",
					Toast.LENGTH_SHORT).show();
		}

	}
}
