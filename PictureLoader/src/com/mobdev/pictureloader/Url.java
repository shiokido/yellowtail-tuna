package com.mobdev.pictureloader;

public class Url {

	private int id;
	
	private String url;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getText() {
		return url;
	}

	public void setText(String text) {
		this.url = text;
	}
	
	public String toString() {
		try {
			String out = url.substring(url.lastIndexOf("/"));
			return out;
		} catch (StringIndexOutOfBoundsException e) {
			// TODO: handle exception
		}
		
		return this.url;
	}
}
