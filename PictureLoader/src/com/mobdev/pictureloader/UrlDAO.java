package com.mobdev.pictureloader;

import java.util.ArrayList;
import java.util.List;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;


public class UrlDAO {

	private SQLiteDatabase db;
	private UrlSQLiteHelper dbHelper;
	
	public UrlDAO(Context context) {
		dbHelper = new UrlSQLiteHelper(context);
		db = dbHelper.getWritableDatabase();
	}
	
	// Close the db
	public void close() {
		db.close();
	}
	

	public void createTodo(String urlText) {
		ContentValues contentValues = new ContentValues();
		contentValues.put("todo", urlText);
	    // Insert into DB
		db.insert("todos", null, contentValues);
	}
	

	public void deleteTodo(int urlId) {
		// Delete from DB where id match
		db.delete("todos", "_id = " + urlId, null);
	}
	

	public List<Url> getTodos() {
		List<Url> urlList = new ArrayList<Url>();
		
		// Name of the columns we want to select
		String[] tableColumns = new String[] {"_id","todo"};
		
		// Query the database
		Cursor cursor = db.query("todos", tableColumns, null, null, null, null, null);
		cursor.moveToFirst();
		
		// Iterate the results
	    while (!cursor.isAfterLast()) {
	    	Url url = new Url();
	    	// Take values from the DB
	    	url.setId(cursor.getInt(0));
	    	url.setText(cursor.getString(1));
	    	
	    	// Add to the DB
	    	urlList.add(url);
	    	
	    	// Move to the next result
	    	cursor.moveToNext();
	    }
		
		return urlList;
	}
	
	
	
}
